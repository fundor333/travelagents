#!/usr/bin/env python
# encoding: utf-8
from gnr.app.gnrdbo import GnrDboTable, GnrDboPackage

class Package(GnrDboPackage):
    def config_attributes(self):
        return dict(comment='travel package',sqlschema='travel',sqlprefix=True,
                    name_short='Travel', name_long='Travel Agents', name_full='Travel')
                    
    def config_db(self, pkg):
        pass
        
class Table(GnrDboTable):
    pass
