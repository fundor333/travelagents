# encoding: utf-8

class Table(object):
    def config_db(self,pkg):
        tbl =  pkg.table('op_specializzazione',pkey='codice',name_long='Specializzazione operatore',name_plural='Specializzazioni operatore',caption_field='descrizione',lookup=True)
        self.sysFields(tbl,id=False)
        tbl.column('codice',size=':6',name_long='Codice')
        tbl.column('descrizione',name_long='Descrizione')
