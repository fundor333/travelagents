# encoding: utf-8

class Table(object):
    def config_db(self,pkg):
        tbl =  pkg.table('operatore',pkey='id',name_long='Operatore',name_plural='Operatori',caption_field='denominazione')
        self.sysFields(tbl)
        tbl.column('codice',size=':10',name_long='Codice',unique=True,indexed=True,validate_notnull=True,validate_case='u')
        tbl.column('denominazione',name_long='Denominazione',validate_notnull=True)
        tbl.column('website',name_long='Website')
        tbl.column('nazione',size=':2',name_long='Nazione')
        tbl.column('email',name_long='Email Principale')
        tbl.column('telefono',name_long='Telefono')
        tbl.column('fax',name_long='Fax')
        tbl.column('logo',name_long='Logo')
        tbl.column('note',name_long='Note')
        tbl.column('aree_servite',name_long='Aree geografiche')
