# encoding: utf-8

class Table(object):
    def config_db(self,pkg):
        tbl =  pkg.table('area_geo',pkey='id',name_long='Area Geografica',name_plural='Aree Geografiche',caption_field='descrizione')
        self.sysFields(tbl,hierarchical='descrizione',counter=True)
        tbl.column('codice',size=':10',name_long='Codice',name_short='Codice',unique=True,indexed=True)
        tbl.column('descrizione',name_long='Descrizione')
