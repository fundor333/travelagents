#!/usr/bin/python
# -*- coding: UTF-8 -*-

from gnr.web.gnrbaseclasses import BaseComponent
from gnr.core.gnrdecorator import public_method

class View(BaseComponent):

    def th_struct(self,struct):
        r = struct.view().rows()
        r.fieldcell('codice')
        r.fieldcell('denominazione')
        r.fieldcell('website')
        r.fieldcell('nazione')
        r.fieldcell('email')
        r.fieldcell('telefono')
        r.fieldcell('fax')
        r.fieldcell('logo')
        r.fieldcell('note')

    def th_order(self):
        return 'codice'

    def th_query(self):
        return dict(column='denominazione', op='contains', val='')



class Form(BaseComponent):

    def th_form(self, form):
        bc = form.center.borderContainer()
        top = bc.contentPane(region='top',datapath='.record')
        fb = top.formbuilder(cols=2, border_spacing='4px')
        fb.field('codice')
        fb.field('denominazione')
        fb.field('website')
        fb.field('nazione')
        fb.field('email')
        fb.field('telefono')
        fb.field('fax')
        fb.field('logo')
        fb.field('note')
        fb.field('aree_servite',tag='checkboxtext',table='travel.area_geo',hierarchical=True,
                popup=True)
        center = bc.borderContainer(region='center')
        left = center.contentPane(region='left',width='50%',splitter=True)
        left.inlineTableHandler(relation='@specializzazioni_operatore',
                                    viewResource='ViewFromOperatore',
                                    picker='specializzazione_codice',pbl_classes=True,margin='2px')

        center_center = center.contentPane(region='center',overflow='hidden')
        center_center.dialogTableHandler(relation='@viaggi',viewResource='ViewFromOperatore',
                                        formResource='FormFromOperatore',pbl_classes=True,margin='2px')


        #center_center.htmliframe(src='^.record.website',height='100%',width='100%',border=0)


    def th_options(self):
        return dict(dialog_height='400px', dialog_width='600px')
