#!/usr/bin/python
# -*- coding: UTF-8 -*-

def config(root,application=None):
    travel = root.branch('Travel Agents')
    travel.thpage('Operatore',table='travel.operatore')
    travel.thpage('Viaggio',table='travel.viaggio')
    travel.thpage('Aree Geografiche',table='travel.area_geo')
    travel.lookups(u"Tabelle Ausiliarie", lookup_manager="travel")
